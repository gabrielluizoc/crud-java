package dao;

import com.mysql.cj.xdevapi.PreparableStatement;
import model.Cultura;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CulturaDAO {
    
    private Connection conn;
    private PreparedStatement prepareStatement;
    private Statement statement;
    private ResultSet resultSet;
    private ArrayList<Cultura> list = new ArrayList<Cultura>();
    
    public CulturaDAO() {
        conn = new ConnectionFactory().getCon();
    }
    
    public void insert(Cultura cultura) {
        String sql = "INSERT INTO culturas(nome, nome_cientifico, descricao) VALUES(?, ?, ?);";
        
        try{
            prepareStatement = conn.prepareStatement(sql);
            prepareStatement.setString(1, cultura.getNome());
            prepareStatement.setString(2, cultura.getNomeCientifico());
            prepareStatement.setString(3, cultura.getDescricao());
            
            prepareStatement.execute();
            prepareStatement.close();   
            
        } catch(SQLException ex) {
            throw new RuntimeException("Error 2:"+ex);
        }
    }
    
    public void update(Cultura cultura) {
        String sql = "UPDATE culturas SET nome = ?, nome_cientifico= ?, descricao = ? WHERE id_cultura = ?;";
        
        try{
            prepareStatement = conn.prepareStatement(sql);
            prepareStatement.setString(1, cultura.getNome());
            prepareStatement.setString(2, cultura.getNomeCientifico());
            prepareStatement.setString(3, cultura.getDescricao());
            prepareStatement.setInt(4, cultura.getIdCultura());
            
            prepareStatement.execute();
            prepareStatement.close();   
            
        } catch(SQLException ex) {
            throw new RuntimeException("Error 3:"+ex);
        }
    }
    
    public void delete(int idCultura) {
        String sql = "DELETE FROM culturas WHERE id_cultura = "+idCultura;
        
        try{
            statement = conn.createStatement();
            
            statement.execute(sql);
            statement.close();   
            
        } catch(SQLException ex) {
            throw new RuntimeException("Error 4:"+ex);
        }
    }
    
    public ArrayList<Cultura> selectAll() {
        String sql = "SELECT * FROM culturas";
        
        try{
            statement = conn.createStatement();
                         
            resultSet = statement.executeQuery(sql);
            
            while(resultSet.next()) {
                Cultura cultura = new Cultura();
                cultura.setIdCultura(resultSet.getInt("id_cultura"));
                cultura.setNome(resultSet.getString("nome"));
                cultura.setNomeCientifico(resultSet.getString("nome_cientifico"));
                cultura.setDescricao(resultSet.getString("descricao"));
                list.add(cultura);
            }
            statement.close();
        } catch(SQLException ex) {
            throw new RuntimeException("Error 5:"+ex);
        }
        return list;
    }
    
    public ArrayList<Cultura> selectByName(String nome) {
        String sql = "SELECT * FROM culturas WHERE nome  LIKE '%"+nome+"%';";
        
        try{
            statement = conn.createStatement();
                         
            resultSet = statement.executeQuery(sql);
            
            while(resultSet.next()) {
                Cultura cultura = new Cultura();
                cultura.setIdCultura(resultSet.getInt("id_cultura"));
                cultura.setNome(resultSet.getString("nome"));
                cultura.setNomeCientifico(resultSet.getString("nome_cientifico"));
                cultura.setDescricao(resultSet.getString("descricao"));
                list.add(cultura);
            }
            statement.close();
        } catch(SQLException ex) {
            throw new RuntimeException("Error 6:"+ex);
        }
        return list;
    }
}
