package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionFactory {

    public String dbURL = "jdbc:mysql://www.agrovgs.com.br:3306/agrovg66_cifarm";
    public String username = "agrovg66_cifarm";
    public String password = "PROJETOcifarm123$";

    public Connection getCon() {
        try {
            return DriverManager.getConnection(this.dbURL, this.username, this.password);
            
        } catch (SQLException ex) {
            throw new RuntimeException("Error :"+ex);
        }
    }
}
