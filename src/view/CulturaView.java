package view;

import dao.CulturaDAO;
import java.awt.event.KeyEvent;
import model.Cultura;
import table.CulturaTableModel;

public class CulturaView extends javax.swing.JFrame {
    
    Cultura cultura = new Cultura();
    CulturaDAO culturaDAO = new CulturaDAO();
 
    public CulturaView() {
        initComponents();
        setLocationRelativeTo(null);
        
        tbCultura.setModel(new CulturaTableModel(new CulturaDAO().selectAll()));
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        tfNome = new javax.swing.JTextField();
        tfNomeCientifico = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        taDescricao = new javax.swing.JTextArea();
        btSalvar = new javax.swing.JButton();
        btExcuir = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbCultura = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        tfPesquisa = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        tfIdCultura = new javax.swing.JTextField();
        tbLimpar = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Culturas");

        jLabel1.setText("Nome");

        jLabel2.setText("Nome Cientifíco");

        jLabel3.setText("Descrição");

        tfNome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfNomeActionPerformed(evt);
            }
        });

        tfNomeCientifico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfNomeCientificoActionPerformed(evt);
            }
        });

        taDescricao.setColumns(20);
        taDescricao.setLineWrap(true);
        taDescricao.setRows(5);
        jScrollPane1.setViewportView(taDescricao);

        btSalvar.setText("Salvar");
        btSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSalvarActionPerformed(evt);
            }
        });

        btExcuir.setText("Excluir");
        btExcuir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btExcuirActionPerformed(evt);
            }
        });

        tbCultura.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbCultura.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbCulturaMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tbCultura);

        jLabel4.setText("Pesquisar");

        tfPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tfPesquisaKeyPressed(evt);
            }
        });

        jLabel5.setText("Código");

        tfIdCultura.setEditable(false);

        tbLimpar.setText("Limpar");
        tbLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tbLimparActionPerformed(evt);
            }
        });

        jButton1.setText("Atualizar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 561, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfNome))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfNomeCientifico))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfIdCultura, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tbLimpar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btExcuir)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btSalvar))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfPesquisa)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(tfIdCultura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(tfNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(tfNomeCientifico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btSalvar)
                    .addComponent(btExcuir)
                    .addComponent(tbLimpar)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(tfPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tfNomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfNomeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfNomeActionPerformed

    private void tfNomeCientificoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfNomeCientificoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfNomeCientificoActionPerformed

    private void btSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSalvarActionPerformed
        if(tfIdCultura.getText().equals("")) {
            cultura.setNome(tfNome.getText());
            cultura.setNomeCientifico(tfNomeCientifico.getText());
            cultura.setDescricao(taDescricao.getText());
            culturaDAO.insert(cultura);            
        } else {
            cultura.setNome(tfNome.getText());
            cultura.setNomeCientifico(tfNomeCientifico.getText());
            cultura.setDescricao(taDescricao.getText());
            cultura.setIdCultura(Integer.parseInt(tfIdCultura.getText()));
            culturaDAO.update(cultura);          
        }
        tbCultura.setModel(new CulturaTableModel(new CulturaDAO().selectAll()));
        tfIdCultura.setText("");
        tfNome.setText("");
        tfNomeCientifico.setText("");
        taDescricao.setText("");
        tfPesquisa.setText("");        
    }//GEN-LAST:event_btSalvarActionPerformed

    private void tbCulturaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbCulturaMouseClicked
        // TODO add your handling code here:
        tfIdCultura.setText(tbCultura.getValueAt(tbCultura.getSelectedRow(), CulturaTableModel.COL_ID_CULTURA).toString());
        tfNome.setText(tbCultura.getValueAt(tbCultura.getSelectedRow(), CulturaTableModel.COL_NOME).toString());
        tfNomeCientifico.setText(tbCultura.getValueAt(tbCultura.getSelectedRow(), CulturaTableModel.COL_NOME_CIENTIFICO).toString());
        taDescricao.setText(tbCultura.getValueAt(tbCultura.getSelectedRow(), CulturaTableModel.COL_DESCRICAO).toString());
    }//GEN-LAST:event_tbCulturaMouseClicked

    private void tbLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tbLimparActionPerformed
        // TODO add your handling code here:
        tbCultura.setModel(new CulturaTableModel(new CulturaDAO().selectAll()));
        tfIdCultura.setText("");
        tfNome.setText("");
        tfNomeCientifico.setText("");
        taDescricao.setText("");
        tfPesquisa.setText("");  
    }//GEN-LAST:event_tbLimparActionPerformed

    private void btExcuirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btExcuirActionPerformed
        // TODO add your handling code here:
        int idCultura = Integer.parseInt(tfIdCultura.getText());
        culturaDAO.delete(idCultura); 
        tbCultura.setModel(new CulturaTableModel(new CulturaDAO().selectAll()));
        tfIdCultura.setText("");
        tfNome.setText("");
        tfNomeCientifico.setText("");
        taDescricao.setText("");
        tfPesquisa.setText("");
    }//GEN-LAST:event_btExcuirActionPerformed

    private void tfPesquisaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfPesquisaKeyPressed
        // TODO add your handling code here:
        String pesquisa = tfPesquisa.getText();
        if(evt.getKeyCode() == KeyEvent.VK_ENTER) {
            tbCultura.setModel(new CulturaTableModel(new CulturaDAO().selectByName(pesquisa)));
        }                
    }//GEN-LAST:event_tfPesquisaKeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        tbCultura.setModel(new CulturaTableModel(new CulturaDAO().selectAll()));
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btExcuir;
    private javax.swing.JButton btSalvar;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea taDescricao;
    private javax.swing.JTable tbCultura;
    private javax.swing.JButton tbLimpar;
    private javax.swing.JTextField tfIdCultura;
    private javax.swing.JTextField tfNome;
    private javax.swing.JTextField tfNomeCientifico;
    private javax.swing.JTextField tfPesquisa;
    // End of variables declaration//GEN-END:variables
}
