package table;

import javax.swing.table.AbstractTableModel;
import model.Cultura;
import java.util.ArrayList;

public class CulturaTableModel extends AbstractTableModel {
    
    public static final int COL_ID_CULTURA = 0;
    public static final int COL_NOME = 1;
    public static final int COL_NOME_CIENTIFICO = 2;
    public static final int COL_DESCRICAO = 3;
    
    public ArrayList<Cultura> list;
    
    public CulturaTableModel(ArrayList<Cultura>l) {
        list = new ArrayList<Cultura>(l);
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
       return 4;
    }

    @Override
    public Object getValueAt(int lines, int cols) {
        Cultura cultura = list.get(lines);
        
        if(cols == COL_ID_CULTURA) return cultura.getIdCultura();
        if(cols == COL_NOME) return cultura.getNome();
        if(cols == COL_NOME_CIENTIFICO) return cultura.getNomeCientifico();
        if(cols == COL_DESCRICAO) return cultura.getDescricao();
        
        return "";
    }
    
    @Override
    public String getColumnName(int cols) {
        if(cols == COL_ID_CULTURA) return "Código";
        if(cols == COL_NOME) return "Nome";
        if(cols == COL_NOME_CIENTIFICO) return "Nome Científico";
        if(cols == COL_DESCRICAO) return "Descrição";
        return "";
    }
    
}
